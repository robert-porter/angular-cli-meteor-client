import {Component, NgZone} from "@angular/core";
import User = Meteor.User;
import {DemoCollection} from "./both/collections/demo.collection";
import {MeteorObservable} from "meteor-rxjs";

@Component({
    selector: "app",
    templateUrl: "./app.component.html",
    styleUrls: ["app.component.css"]
})
export class AppComponent {
    public serverResponse: any;

    constructor(private zone: NgZone) {
        Tracker.autorun(() => {
            if (Meteor.user()) {
                this.zone.run(() => {
                    this.user = Meteor.user();
                });
            }
        })
    }

    user: User;

    public logout() {
        Meteor.logout(err => {
            if (err) {
                console.log(err);
            } else {
                this.zone.run(() => {
                    this.user = Meteor.user();
                });
                console.log("Logged Out")
            }
        });
    }

    public login() {
        Meteor.loginWithPassword("foo@bar.com", "foobar", err => {
            if (err) {
                console.log(err);
            } else {
                console.log("Logged in")
            }
        });
    }

    public forgotPassword() {
        Accounts.forgotPassword({"email": "foo@bar.com"});
    }

    public createDemo() {
        let newDemo: any = {
            name: "Rob",
            age: 17
        };

        DemoCollection.collection.insert(newDemo, (err) => {
            if (err)
                console.log(err);
        });
    }

    public throwServerError() {
        MeteorObservable.call("throwError", "Error from Angular-Cli Client").subscribe(
            res => {
                this.zone.run(() => {this.serverResponse = res?res:"Success"})
            }, err => {
                this.zone.run(() => {this.serverResponse = err})
            }
        )
    }
}
