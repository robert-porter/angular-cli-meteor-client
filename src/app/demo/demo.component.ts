import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs";
import { DemoDataService } from "./demo-data.service";
import {Demo} from "../both/models/demo.model";

@Component({
  selector: "demo",
  templateUrl: "./demo.component.html",
  styleUrls: [ "./demo.component.css" ]
})
export class DemoComponent implements OnInit {
  greeting: string;
  data: Observable<Demo[]>;

  constructor(private demoDataService: DemoDataService) {
    this.greeting = "Hello Demo Component!";
  }

  ngOnInit() {
    this.data = this.demoDataService.getData().zone();
  }
}
