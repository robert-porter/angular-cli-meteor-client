import 'meteor-client';		// First load Meteor client Atmosphere packages bundled with `meteor-client-bundler`
import 'angular2-meteor-polyfills';		// Next load needed Meteor and Angular shims prior to other Angular imports

import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule);
