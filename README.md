# Dev Build Instructions

1. Git Clone
2. cd new directory
3. meteor npm install
4. meteor-client bundle --config=meteor-client.config.json
5. Modify meteor-client to remove "Mongo is not defined" error.
    - Open node_modules/meteor-client.js.
    - Search for 'Imports for global scope' 
    - Add the line "Mongo = Package.mongo.Mongo;" just below. 
6. ng serve


# Production Build 

1. ng build --environment=prod --output-path dist2 --prod --no-extract-license
2. Install Nginx and point a virtual host to the dist2 directory to serve these files.

NB: I build into dist2 directory so that it doesn't clash with ng serve.


