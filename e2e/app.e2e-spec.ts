import { MeteorAppPage } from './app.po';

describe('meteor-app App', () => {
  let page: MeteorAppPage;

  beforeEach(() => {
    page = new MeteorAppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
